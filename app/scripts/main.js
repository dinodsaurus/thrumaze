"use strict";
var app = (function () {
  var fixNav = function () {
    var $el = $('.fixHead');
    var $sub = $('.homeHead');
    $(window).scroll(function () {
      var top = $(this).scrollTop();
      if(top > 90 && !$el.hasClass('active')){
        $el.addClass('active');
        $sub.addClass('active');
      }else if( top < 90 && $el.hasClass('active')){
        $el.removeClass('active');
        $sub.removeClass('active');
      }
    });
  },
  subnav = function () {
    $(".homeHead a").click(function (e) {
      e.preventDefault();
      var top = $(this).attr("href");
      var scroll = $("#" + top).offset();
      $('html, body').animate({
          scrollTop: scroll.top - 50
      }, 500);
    });
  },
  formSubmit = function () {
    $( "#contact" ).submit(function( event ) {
      event.preventDefault();
      if(!("#email").val()){
        $( "#contact" ).submit();
      }
    });
  };
  return {
    fixNav: fixNav,
    formSubmit: formSubmit,
    subnav: subnav
  };
})();
